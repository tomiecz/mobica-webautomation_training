package part3;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by tomi on 2015-03-09.
 */
public class Part3FinalExercisesTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test
    public void should_assert_title(){
        //should pass
        driver = new FirefoxDriver();
        driver.get(BASE_URL);
        assertEquals("Page title is not correct", "Login : Mobica-crm", driver.getTitle());
        driver.quit();
    }

    @Test
    public void should_assert_url(){
        //should pass
        driver = new FirefoxDriver();
        driver.get(BASE_URL);
        assertTrue("Page URL is not correct",
                driver.getCurrentUrl().equals(BASE_URL + "login"));

        assertThat("Page URL is incorrect", driver.getCurrentUrl(), is(BASE_URL + "login"));

        driver.quit();
    }
}
