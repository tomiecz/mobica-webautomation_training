package part3;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Created by tomi on 2015-03-09.
 */
public class JUnitAssertionsTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test
    public void should_make_different_assertions() {

        String nulltest = null;

        driver = new FirefoxDriver();
        driver.get("https://mobica-crm-training.herokuapp.com/login");

        //should result true
        assertEquals("Page title is not correct", "Login : Mobica-crm", driver.getTitle());

        // should result true
        assertNotEquals("Page title is not correct", "Login: Mobica-crm", driver.getTitle());

        // should result true
        assertTrue("Page url is not correct", driver.getCurrentUrl().equals(BASE_URL + "login"));

        //should result true
        assertFalse("Page url is not correct", driver.getCurrentUrl().equals("test"));

        //should result true
        assertThat("Page title is not correct", "Login : Mobica-crm", is(driver.getTitle()));

        //should result true
        assertNull("Object is not null", nulltest);

        //should result true
        assertNotNull("Object is null", driver);

        driver.quit();
    }
}
