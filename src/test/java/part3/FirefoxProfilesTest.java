package part3;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.io.File;
import java.io.IOException;

/**
 * Created by tomi on 2015-03-09.
 */
public class FirefoxProfilesTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test
    public void should_create_FF_profile(){

        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.startup.homepage", "http:www.mobica.com");
        profile.setPreference("browser.display.background_color", "#FF0000");

        driver = new FirefoxDriver(profile);
        driver.quit();
    }

    @Test
    public void should_install_firebug_from_file() throws IOException {

        final String FIREBUG_EXEC = "src/test/resources/ffAddOns/firebug-2.0.7-fx.xpi";

        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.startup.homepage", BASE_URL);

        File file = new File(FIREBUG_EXEC);
        profile.addExtension(file);

        driver = new FirefoxDriver(profile);

        driver.quit();
    }
}
