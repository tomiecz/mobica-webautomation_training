package part6;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Created by tomi on 2015-03-09.
 */
public class CreateCompanyTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";
    private String companyName;
    private String companyUser;

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "src/test/resources/webdrivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(BASE_URL);
        driver.findElement(By.id("login")).sendKeys("tomasz.miecznikowski@mobica.com");
        driver.findElement(By.id("password")).sendKeys("Test.123");
        driver.findElement(By.className("submit-button")).click();
    }

    @Test
    public void should_create_company() throws InterruptedException {

        companyName = "Mobica";
        String address = "ul. Wólczańska, Łódź";
        String companyVertical = "Intel - ISS";
        String companyCountry = "Poland";
        String companyTimezone = "(GMT+01:00) Bern";
        String companyStatus = "New";
        String companyPhone = "+48000111222";
        String companyDescription = "Mobica is a leading provider of software engineering, testing and consultancy services";
        String numberOfEmployees = "700";
        String numberOfDevs = "400";
        String companyPriority = "MEDIUM";
        companyUser = "Tomasz Miecznikowski";

        driver.findElement(By.linkText("Companies")).click();
        driver.findElement(By.partialLinkText("New Company")).click();

        driver.findElement(By.id("company_name")).sendKeys(companyName);

        Select vertical = new Select(driver.findElement(By.id("company_vertical")));
        vertical.selectByVisibleText(companyVertical);

        driver.findElement(By.id("company_address")).sendKeys(address);

        Select country = new Select(driver.findElement(By.id("company_country")));
        country.selectByVisibleText(companyCountry);

        Select timezone = new Select(driver.findElement(By.id("company_time_zone")));
        timezone.selectByValue(companyTimezone);

        Select status = new Select(driver.findElement(By.id("company_status")));
        status.selectByVisibleText(companyStatus);

        driver.findElement(By.id("company_phone")).sendKeys(companyPhone);
        driver.findElement(By.id("company_invoice_address")).sendKeys(address);
        driver.findElement(By.id("company_description")).sendKeys(companyDescription);

        WebElement numOfEmployees = driver.findElement(By.id("company_number_of_employees"));
        numOfEmployees.clear();
        numOfEmployees.sendKeys(numberOfEmployees);

        WebElement numOfDevs = driver.findElement(By.id("company_number_of_devs"));
        numOfDevs.clear();
        numOfDevs.sendKeys(numberOfDevs);

        Select priority = new Select(driver.findElement(By.id("company_priority")));
        priority.selectByValue(companyPriority);

        Select user = new Select(driver.findElement(By.id("company_user_id")));
        user.selectByVisibleText(companyUser);

        //FORM SUBMIT
        driver.findElement(By.id("company_submit")).click();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("Company : Mobica-crm"));

        //TITLE CHECK
        assertEquals("Tytuł jest niepoprawny", "Company : Mobica-crm", driver.getTitle());

        //SUCCESS MSG CHECK
        assertTrue("There is no success msg displayed", driver.findElement(By.xpath("/html/body/div[3]")).isDisplayed());
        assertEquals("The success msg is incorrect", "The company was created successfully", driver.findElement(By.xpath("/html/body/div[3]")).getText());

        //COMPANY ATTRIBUTES CHECK
        assertEquals("The company name is not correct", companyName, driver.findElement(By.className("company-name")).getText());
        assertEquals("The company country is not correct", companyCountry, driver.findElement(By.xpath("/html/body/div[4]/div[2]/div/div/table/tbody/tr[5]/td/span")).getText());
    }

    @After
    public void tearDown(){

        //CLEANUP
        driver.findElement(By.linkText("Companies")).click();

        WebElement table = driver.findElement(By.className("table-plus"));

        List<WebElement> tableRows = table.findElements(By.tagName("tr"));

        for (WebElement tableRow : tableRows.subList(1, tableRows.size() - 1)) {
            List<WebElement> rowCells = tableRow.findElements(By.tagName("td"));

            if (rowCells.get(0).getText().equals(companyName) && rowCells.get(11).getText().equals(companyUser)){
                tableRow.findElement(By.className("remove")).click();

                Alert confAlert = driver.switchTo().alert();
                confAlert.accept();

                break;
            }
        }

        //DRIVER QUIT
        driver.quit();
    }
}
