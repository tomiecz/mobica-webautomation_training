package part5;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by tomi on 2015-03-09.
 */
public class ElementsAttributesTest {

    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";
    private WebElement loginInput;
    private WebElement pwdInput;
    private WebElement submitBtn;

    @Before
    public void setUp() {
        driver = new FirefoxDriver();
        driver.get(BASE_URL);
    }

    @Test
    public void should_see_correct_error_message() {

        loginInput = driver.findElement(By.id("login"));
        pwdInput = driver.findElement(By.id("password"));
        submitBtn = driver.findElement(By.xpath("/html/body/div[3]/div[2]/form/div[2]/input"));

        loginInput.sendKeys("test");
        pwdInput.sendKeys("test");
        submitBtn.click();

        WebElement errorMsg = driver.findElement(By.className("error"));

        assertEquals("The error msg is not correct", "You did not provide a valid Email address and Password!",
                errorMsg.getText());

        assertEquals("The error message bg color is incorrect", "rgba(188, 28, 61, 1)",
                errorMsg.getCssValue("background-color"));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
