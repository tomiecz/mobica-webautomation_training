package part5;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by tomi on 2015-03-09.
 */
public class ActionsTest {
    private WebDriver driver = new FirefoxDriver();
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test
    public void should_create_actions() {

        driver.get("http://www.mobica.com");
        WebElement element = driver.findElement(By.id("maincontainer"));
        Actions action = new Actions(driver);
        action.contextClick(element).build().perform();
        driver.quit();
    }

    @Test
    public void should_drag_and_drop(){
        driver.get("http://marcojakob.github.io/dart-dnd/basic/web/");
        Actions builder = new Actions(driver);

        WebElement trash = driver.findElement(By.className("trash"));

        builder.dragAndDrop(driver.findElement(By.xpath("/html/body/div/img[1]")),trash)
                .dragAndDrop(driver.findElement(By.xpath("/html/body/div/img[2]")), trash)
                .dragAndDrop(driver.findElement(By.xpath("/html/body/div/img[3]")),trash)
                .dragAndDrop(driver.findElement(By.xpath("/html/body/div/img[4]")),trash);

        Action dragdrop = builder.build();

        dragdrop.perform();
        driver.quit();
    }

    @Test
    public void should_change_menu_style(){
        driver.manage().window().maximize();
        driver.get("https://mobica-crm-training.herokuapp.com/");
        driver.findElement(By.id("login")).sendKeys("tomasz.miecznikowski@mobica.com");
        driver.findElement(By.id("password")).sendKeys("Test.123");
        driver.findElement(By.className("submit-button")).click();

        new MouseHoverAction().mouseHover(driver, driver.findElement(By.linkText("Companies")));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}

class MouseHoverAction {

    public void mouseHover(WebDriver driver, WebElement element){
        Actions builder = new Actions(driver);
        builder.moveToElement(element).perform();
    }
}
