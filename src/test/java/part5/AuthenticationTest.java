package part5;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by tomi on 2015-03-09.
 */
public class AuthenticationTest {

    private final String email = "tomasz.miecznikowski@mobica.com";
    private final String pass = "Test.123";
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";
    private WebElement loginInput;
    private WebElement pwdInput;
    private WebElement submitBtn;

    @Before
    public void setUp(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(BASE_URL);
    }

    @Test
    public void should_login_correctly(){

        loginInput = driver.findElement(By.id("login"));
        pwdInput = driver.findElement(By.id("password"));
        submitBtn = driver.findElement(By.className("submit-button"));

        loginInput.sendKeys(email);
        pwdInput.sendKeys(pass);
        submitBtn.click();

        assertEquals(driver.getTitle(), "Home : Mobica-crm");
        assertEquals(driver.getCurrentUrl(), BASE_URL);

        assertTrue(driver.findElement(By.className("front-page")).isDisplayed());

        Set<Cookie> cookies = driver.manage().getCookies();
        Cookie loginCookie = driver.manage().getCookieNamed("auth_token");

        assertTrue(cookies.contains(loginCookie));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
