package part2;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by tomi on 2015-03-09.
 */
public class DriverNavigationTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test
    public void should_navigate_to_page_then_refresh_and_go_back_then_forward(){

        driver = new FirefoxDriver();

        driver.navigate().to(BASE_URL);
        System.out.println("URL loaded");

        driver.navigate().refresh();
        System.out.println("URL refreshed");

        driver.navigate().back();
        System.out.println("Step back");

        driver.navigate().forward();
        System.out.println("Step forward");

        driver.quit();
    }
}
