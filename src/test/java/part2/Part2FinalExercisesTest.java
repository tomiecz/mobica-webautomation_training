package part2;

import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by tomi on 2015-03-09.
 */
public class Part2FinalExercisesTest {
    private final String CHROME_EXEC = "src/test/resources/webdrivers/chromedriver.exe";
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test
    public void should_set_up_window(){
        System.setProperty("webdriver.chrome.driver", CHROME_EXEC);
        driver = new ChromeDriver();

        driver.get(BASE_URL + "login");
        driver.manage().window().maximize();

        Dimension dimensions = driver.manage().window().getSize();
        System.out.println("Webdriver started with resolution: " + dimensions.toString());

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        driver.quit();
    }

    @Test
    public void should_get_window_info(){
        System.setProperty("webdriver.chrome.driver", CHROME_EXEC);
        WebDriver driver = new ChromeDriver();

        driver.get("https://mobica-crm-training.herokuapp.com/login");
        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
        System.out.println(driver.getPageSource());
        driver.quit();
    }

}
