package part2;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by tomi on 2015-03-09.
 */
public class DriverQuitCloseTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test
    public void should_close_just_one_window(){
        driver = new FirefoxDriver();
        driver.get(BASE_URL);
        driver.findElement(By.partialLinkText("Forgotten")).sendKeys(Keys.LEFT_SHIFT, Keys.ENTER);
        driver.close();
    }

    @Test
    public void should_close_all_windows(){
        driver = new FirefoxDriver();
        driver.get(BASE_URL);
        driver.findElement(By.partialLinkText("Forgotten")).sendKeys(Keys.LEFT_SHIFT, Keys.ENTER);
        driver.quit();
    }
}
