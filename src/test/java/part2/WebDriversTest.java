package part2;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * Created by tomi on 2015-03-08.
 */

public class WebDriversTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";


    @Test
    public void should_open_FF_driver(){
        driver = new FirefoxDriver();
        driver.get(BASE_URL);
        driver.quit();
    }

    @Test
    public void should_open_Chrome_driver(){
        System.setProperty("webdriver.chrome.driver", "src/test/resources/webdrivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(BASE_URL);
        driver.quit();
    }

    @Test
    public void should_open_HtmlUnit_driver(){
        driver = new HtmlUnitDriver();
        driver.get(BASE_URL);
        driver.quit();
    }

    @Test
    public void should_open_IE_driver(){
        System.setProperty("webdriver.ie.driver", "src/test/resources/webdrivers/IEDriverServer.exe");
        driver = new InternetExplorerDriver();
        driver.get(BASE_URL);
        driver.quit();
    }

    @Test
    public void should_open_Opera_driver(){
        System.setProperty("webdriver.chrome.driver", "src/test/resources/webdrivers/operadriver.exe");
        driver = new ChromeDriver();
        driver.get(BASE_URL);
        driver.quit();
    }
}
