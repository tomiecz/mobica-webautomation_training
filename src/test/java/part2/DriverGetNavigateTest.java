package part2;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by tomi on 2015-03-09.
 */
public class DriverGetNavigateTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test
    public void should_get_login_page(){
        driver = new FirefoxDriver();
        driver.get(BASE_URL);
        driver.close();
    }

    @Test
    public void should_navigate_to_page(){
        driver = new FirefoxDriver();
        driver.navigate().to(BASE_URL);
        driver.quit();
    }
}
