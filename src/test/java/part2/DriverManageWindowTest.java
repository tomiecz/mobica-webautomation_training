package part2;

import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by tomi on 2015-03-09.
 */
public class DriverManageWindowTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test
    public void should_manage_window(){

        System.setProperty("webdriver.chrome.driver", "src/test/resources/webdrivers/chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        //will maximize browser window
        driver.manage().window().maximize();

        //will get and display browser's resolution
        Dimension browserSize = driver.manage().window().getSize();
        System.out.println("Browser resolution is: " + browserSize.toString());

        //will get and display browser's center position
        Point center = driver.manage().window().getPosition();
        System.out.println("Browser center point is: " + center.toString());

        //Set up new browser size
        Dimension newSize = new Dimension(600,600);
        driver.manage().window().setSize(newSize);
        System.out.println("Browser resolution is: " + newSize.toString());

        //Set up new browser center point (position)
        Point newPosition = new Point(5,5);
        driver.manage().window().setPosition(newPosition);
        System.out.println("Browser center point is: " + newPosition.toString());

        driver.quit();
    }
}
