package part2;

import org.junit.Test;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by tomi on 2015-03-09.
 */
public class PageLoadTimeoutExampleTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test
    public void should_change_page_load_timeout() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(2, TimeUnit.SECONDS);

        try {
            driver.get(BASE_URL);
        } catch (TimeoutException e) {
            System.out.println("The page " + BASE_URL + " has been loaded too slow");
        }
        driver.quit();
    }
}
