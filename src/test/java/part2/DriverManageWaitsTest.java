package part2;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by tomi on 2015-03-09.
 */
public class DriverManageWaitsTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test
    public void should_set_up_global_waits(){

        driver = new FirefoxDriver();

        //set up the maximum time for "findElement(s)" methods
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //set up the maximum time for "get" and "navigate" methods
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        //set up the maximum time for asynchronous script to load
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);

        driver.quit();
    }
}
