package part2;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by tomi on 2015-03-09.
 */
public class DriverGetWebDriverDataTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test
    public void should_print_page_title_url_source(){
        driver = new FirefoxDriver();

        driver.get(BASE_URL);

        //get current page title
        String pageTitle = driver.getTitle();

        //get current page url
        String pageUrl = driver.getCurrentUrl();

        // current page source code
        String pageSource = driver.getPageSource();

        System.out.println("Page title is: " + pageTitle);
        System.out.println("Page url is: " + pageUrl);
        System.out.println("Page source is: " + pageSource);

        driver.quit();
    }
}
