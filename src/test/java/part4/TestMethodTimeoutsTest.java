package part4;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by tomi on 2015-03-09.
 */
public class TestMethodTimeoutsTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test(timeout = 10000)
    public void testcase_should_take_no_more_than_10s(){
        driver = new FirefoxDriver();
        driver.get(BASE_URL);
    }

    @After
    public void tearDown(){
        driver.quit();
    }
}
