package part4;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.assertEquals;

/**
 * Created by tomi on 2015-03-09.
 */
public class SendKeysTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test
    public void should_send_keys_to_input_fields(){

        driver = new FirefoxDriver();
        driver.get(BASE_URL);

        WebElement emailInput = driver.findElement(By.id("login"));
        emailInput.sendKeys("login");

        WebElement passwordInput = driver.findElement(By.id("password"));
        passwordInput.sendKeys("password");

        passwordInput.sendKeys(Keys.ENTER);

        assertEquals("Page title is incorrect", "Login : Mobica-crm", driver.getTitle());
        driver.quit();
    }
}
