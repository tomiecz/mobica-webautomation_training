package part4;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

/**
 * Created by tomi on 2015-03-09.
 */
public class FindsTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Test
    public void should_find_different_page_elements(){
        driver = new FirefoxDriver();
        driver.get(BASE_URL);

        WebElement loginInput = driver.findElement(By.id("login"));

        WebElement passwordInput = driver.findElement(By.name("password"));

        WebElement submitButton = driver.findElement(By.className("submit-button"));

        WebElement forgotPwdLink = driver.findElement(By.linkText("Forgotten password?"));

        WebElement forgotPwdLink2 = driver.findElement(By.partialLinkText("Forgotten"));

        WebElement rememberMeCheckbox = driver.findElement(By.xpath("//*[@id=\"remember-me\"]"));

        WebElement header = driver.findElement(By.cssSelector("body > div.header.page-header"));

        WebElement loginText = driver.findElement(By.tagName("h2"));

        driver.quit();
    }

    @Test
    public void should_get_list_of_elements(){

        driver = new FirefoxDriver();
        driver.get(BASE_URL);

        List<WebElement> inputFields = driver.findElements(By.tagName("input"));

        System.out.println(inputFields.size());

        driver.quit();

    }
}
