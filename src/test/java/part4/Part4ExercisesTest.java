package part4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by tomi on 2015-03-09.
 */
public class Part4ExercisesTest {

    private final String email = "tomasz.miecznikowski@mobica.com";
    private final String pass = "Test.123";
    private final String CHROME_EXEC = "src/test/resources/webdrivers/chromedriver.exe";
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";
    private WebElement loginInput;
    private WebElement pwdInput;
    private WebElement submitBtn;
    private WebElement forgotPwdLink;

    @Before
    public void setUp() {

        System.setProperty("webdriver.chrome.driver", CHROME_EXEC);
        driver = new ChromeDriver();

        driver.manage().window().maximize();
        driver.navigate().to(BASE_URL + "login");
        loginInput = driver.findElement(By.id("login"));
        pwdInput = driver.findElement(By.id("password"));
        submitBtn = driver.findElement(By.className("submit-button"));
        forgotPwdLink = driver.findElement(By.partialLinkText("Forgotten"));
    }

    @Test
    public void should_login_correctly(){

        loginInput.sendKeys(email);
        pwdInput.sendKeys(pass);
        submitBtn.click();

        assertEquals("Home : Mobica-crm", driver.getTitle());
        assertEquals(BASE_URL, driver.getCurrentUrl());
        assertTrue(driver.findElement(By.className("front-page")).isDisplayed());
    }

    @Test
    public void should_display_wrong_login_error_message(){

        loginInput.sendKeys("test");
        pwdInput.sendKeys("test");
        submitBtn.click();

        WebElement errorMsg = driver.findElement(By.className("error"));
        assertEquals("You did not provide a valid Email address and Password!", errorMsg.getText());
        assertTrue(errorMsg.getCssValue("background").contains("rgb(188, 28, 61)"));

        assertEquals(BASE_URL + "login", driver.getCurrentUrl());
        assertEquals("Login : Mobica-crm", driver.getTitle());
    }

    @Test
    public void should_open_forgotten_password_page(){

        forgotPwdLink.click();
        assertEquals(BASE_URL + "forgot_password", driver.getCurrentUrl());
        assertEquals("Forgotten password? : Mobica-crm", driver.getTitle());
    }

    @After
    public void tearDown() {

        driver.quit();
    }

}
