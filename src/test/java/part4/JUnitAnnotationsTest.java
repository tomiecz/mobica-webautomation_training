package part4;

import org.junit.*;

/**
 * Created by tomi on 2015-03-09.
 */
public class JUnitAnnotationsTest {

    @BeforeClass
    public static void global_set_up(){
        System.out.println("BeforeClass method executed");
    }

    @Before
    public void set_up(){
        System.out.println("Before method executed");
    }

    @Test
    public void should_execute_first_test(){
        System.out.println("First test executed");
    }

    @Test
    public void should_execute_second_test(){
        System.out.println("Second test executed");
    }

    @Test @Ignore
    public void should_execute_third_test(){
        System.out.println("Third test executed");
    }

    @After
    public void tear_down(){
        System.out.println("After method executed");
    }

    @AfterClass
    public static void global_tear_down(){
        System.out.println("AfterClass method executed");
    }
}
