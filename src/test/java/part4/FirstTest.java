package part4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by tomi on 2015-03-09.
 */
public class FirstTest {
    private WebDriver driver;
    private final String BASE_URL = "https://mobica-crm-training.herokuapp.com/";

    @Before
    public void setUp(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        System.out.println("WebDriver " + driver.getClass().getSimpleName() + " started with reolution: "
                + driver.manage().window().getSize());
        driver.navigate().to(BASE_URL + "login");
    }

    @Test
    public void should_see_login_page(){
        String title = driver.getTitle();
        String url = driver.getCurrentUrl();

        assertEquals("Tytul jest niepoprawny", "Login : Mobica-crm", title);
        assertEquals("Adres URL jest niepoprawny", BASE_URL + "login", url);
    }

    @After
    public void tearDown(){
        System.out.println("Shutting down webdriver...");
        driver.quit();

    }
}
